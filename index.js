// console.log("Happy Monday");

// An array in programming is simply a list of data. Data that are related/connected with each other."

let studentNumberA = "2020-1923";
let studentNumberB = "2020-1924";
let studentNumberC = "2020-1925";
let studentNumberD = "2020-1936";
let studentNumberE = "2020-1937";


let studentsNumbers = ["2020-1923", "2020-1924", "2020-1925", "2020-1936", "2020-1937"];

// [Section] Array
/* 
    - arrays are used to store multiple related values in a single variable.
    - they are delcared using square brackets([]) also knows as "Array Literals"
    - array also provide access to a number of function/methods that help achieving specific task.
    - method is another term for functions associated with an object/array and is used to execute statements that are relevant.
    - majority of methods are used to manipulate information stores within the same object.
    - array are also object which is another type.
    */

    let grades = [98.5, 94.3, 89.2, 90.1];
    console.log(grades);

    let computerBrands = ["Acer", "Asus", "Lenovo","Samsung"];
    console.log(computerBrands);

    // Alternative way to write arrays
    let myTasks = [
        "drink html", 
        "eat javascript",
        "inhale css",
        "bake sass"]

    // Create an array with values from variables:
    let city1 = "Tokyo";
    let city2 = "Manila";
    let city3 = "New York";

    let cities = [city1, city2, city3];
    console.log(cities);

    console.log(cities.length);
    console.log(typeof cities.length);

    // length property can also set the total number of items in an array, meaning we can actually delete the last item in the array or shorten the array by simply updating the length property of an array.

    console.log(myTasks);
    myTasks.length = myTasks.length-1;
    console.log(myTasks);
    myTasks.length--;
    console.log(myTasks);

    // we cant do the same on the strings
    let fullName = "John Doe";
    console.log(fullName.length);
    fullName.length--;
    console.log(fullName.length);
    console.log(fullName);

// [Section] Reading/accession elements of arrays

    // Accessing array elements is one of the more common task that we do with an array
    // This can be done throough the use of index
    // Each element in an array is associated with it's index/number.

    let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"];

    console.log(lakersLegends[4]);

    console.log(lakersLegends);
    lakersLegends[2] = "Pau Gasol";
    console.log(lakersLegends);

    // Accessing the last element of an array

    let lastElementIndex = lakersLegends[lakersLegends.length-1];
    console.log(lastElementIndex);

    // Adding items into the array without using array methods
        // Adding at the end of the array
        let newArr = [];
        newArr[newArr.length] = "Cloud Strife"
        console.log(newArr);
        newArr[newArr.length] = "Tifa Lockhart";
        console.log(newArr);

    // Looping over an array
        for (let index=0; index<newArr.length; index++) {
            console.log(newArr[index]);
        }

        let numArr = [5, 12, 30, 46, 40];
        for (let index=0; index<numArr.length; index++) {
            if (numArr[index]%5===0) {
                console.log(numArr[index] + " is divisible by 5.");
            } else {
                console.log(numArr[index] + " is not divisible by 5.");
            }
        }

// [Section] Multi dimensional arrays
    // Multidimensional arrays are useful for storing complex data structures
    // a practical application of this is to help visualize/create real world objects

    let chessboard = [
        ["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
        ["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
        ["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
        ["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
        ["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
        ["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
        ["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
        ["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"]
    ]

    console.table(chessboard);
    console.table(chessboard[4][5]);